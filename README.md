# MUSI-VCR3-Single-View-Geometry

Este proyecto cuenta con 2 partes diferenciadas:

## Single View Geometry

Práctica para calcular la altura de una persona a partir de puntos de referencia y rectas.

## Epipolar

Práctica para generar las líneas epipolares a partir del sistema de correspondencia entre 2 puntos.

## Estructura del proyecto

```bash
.
├── Notebooks # Contiene los notebooks y los PDFs
├── common # Contiene el módulo con alguna de las funciones necesarias para realizar el proyecto
└── in # Carpeta donde se encontrarán las imágenes
    ├── epipolar # Para el proyecto de las líneas epipolares
    └── metronomy # Para el proyecto del cálculo de la altura de una persona
```

## Entorno

Primer paso instalar anaconda 3

[Anaconda 3](https://www.anaconda.com/products/individual)

Una vez que se tiene instalado anaconda 3 desde la terminal de anaconda instalar el entorno de python que se encuentra situado en la carpeta del proyecto:

```{bash}
conda env create --file environment.yml
```

Una vez instalado el entorno se puede ejecutar el proyecto mediante Pycharm Pro